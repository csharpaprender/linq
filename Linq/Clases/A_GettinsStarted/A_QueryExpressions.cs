using System;
using System.Collections.Generic;
using System.Linq;
namespace LinqBasics
{
    public class BasicExpressions
    {
        public static void BasicSelect()
        {
            int[] scores = new int[] {97,92,81,60};
            /*------------*/
            //No se ha ejecutado la query ya que la ejecución se debe hacer con un for each
            IEnumerable<int> scoreQuery = 
                from score in scores
                where score > 80
                select score + 4;
            // el foreach ejecuta la query, si se edita el arrar y se ejecuta el
            // foreach el resultado se mostrara basado en el array editado 
            foreach(int i in scoreQuery){
                Console.WriteLine(i);
            }
            /*----------------*/

            /*-------------*/
            //la consulta se ha ejecutado ya que el .ToList fuerza la ejecucion 
            List<int> scoreQueryExecuted = 
                (from score in scores
                where score > 80
                select score).ToList();
            /*-------------*/
        }
    }
}
