using System;
using System.Collections.Generic;
using System.Linq;
namespace LinqBasics
{
    public class IntroToLinq
    {
        public static int[] numbers = new int[7] { 0, 1, 2, 3, 4, 5, 6 };
        public static void ModuleLinq(){
            // The Three Parts of a LINQ Query:
            // 1. Data source.
            

            // 2. Query creation.
            // numQuery is an IEnumerable<int>
            var numQuery =
                from num in numbers
                where (num % 2) == 0
                select num;

            // 3. Query execution.
            foreach (int num in numQuery)
            {
                Console.Write("{0,1} ", num);
            }
        }
    
        public static void ForceExecutionWithAgregateFunction(){
            IEnumerable<int> evenNumQuery = 
                from num in numbers 
                where num%2 == 0
                select num;
            
            Console.WriteLine(evenNumQuery.Count());//se fuerza la ejeucion debido a que la funcion de agregado, requiere un foreac interno
        }

        public static void ForceExecutionWitListArray(){ //se fuerza la ejecución de la query debido a la conversión 
            List<int> evenNum =
                (from num in numbers
                where num%2 == 0
                select num).ToList();

            int[] evenNumArrar = (
                from num in numbers
                where num%2 == 0
                select num
            ).ToArray();
        }
    }
}
