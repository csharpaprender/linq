using System;
using System.Collections.Generic;
using System.Linq;
namespace LinqBasics
{
    public class LinqGeneric
    {
        public static void useA_LinqGeneric(){
            var Client = new List<Customer>{
                new Customer {Name="David",City="Quimbaya"},
                new Customer {Name="Maria",City="___"}
            };
            IEnumerable<Customer> clients = 
                from client in Client
                where client.Name == "David"
                select client;

            List<string> clientsName =(
                from client in Client
                where client.Name == "David"
                select client.Name).ToList();

            List<Customer> clientsF = ( //query forzada
                from client in Client
                where client.Name == "David"
                select client
            ).ToList();
            

        }
    }
    public class Customer
    {
        public string Name {get;set;}
        public string City {get;set;}
    }
}
