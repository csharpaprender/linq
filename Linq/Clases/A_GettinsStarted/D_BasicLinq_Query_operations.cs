using System;
using System.Collections.Generic;
using System.Linq;
namespace LinqBasics
{
    public class BasicsLinq
    {
        static List<Obj> _Clients;
        static List<Dest> _Destino;
        static BasicsLinq(){
            List<Obj> Clients = new List<Obj>{
                new Obj{Name="Davd",Age=26,Num=8,NumString="46"},
                new Obj{Name="DavdB",Age=20,Num=8,NumString="44"},
                new Obj{Name="DavdC",Age=25,Num=8,NumString="42"},
                new Obj{Name="DavdD",Age=23,Num=8,NumString="4"},
                new Obj{Name="DavdE",Age=26,Num=8,NumString="40"},
                new Obj{Name="DavdE",Age=29,Num=8,NumString="46"},
            };
            List<Dest> Destino = new List<Dest>{
                new Dest{Name="Davd",city="qqqq"},
                new Dest{Name="DavdB",city="wwww"},
                new Dest{Name="DavdC",city="eeeee"},
                new Dest{Name="DavdD",city="rrrrr"},
                new Dest{Name="DavdE",city="tttt"},
                new Dest{Name="DavdE",city="yyyyy"},
            };
            _Clients = Clients;
            _Destino = Destino;
        }
        public static void Filtering_linq(){
            IEnumerable<Obj> FilterClients = 
                from client in _Clients
                where client.Age <= 25 
                select client;
            foreach (var client in FilterClients)
            {
                Console.WriteLine(client.Name+" "+client.Age+" "+client.Num+" "+client.NumString);
            }
        }
        public static void Ordering_linq(){
            var OrderingClients =
                from client in _Clients
                where client.Age <= 25 
                orderby client.Age
                select client;
            foreach (var client in OrderingClients)
            {
                Console.WriteLine(client.Name+" "+client.Age+" "+client.Num+" "+client.NumString);
            }
        }
        public static void Grouping_linq(){
            IEnumerable<IGrouping<int,Obj>> GroupClients = //es un entero el parametro debido a que la key es entera
            //var GroupClients =
                from client in _Clients
                group client by client.Age;
            foreach(IGrouping<int,Obj> groupClient in GroupClients){ //var = IGrouping<string,Obj>
                Console.WriteLine(groupClient.Key);
                foreach(Obj client in groupClient){
                    Console.WriteLine(client.Name+" "+client.Age+" "+client.Num+" "+client.NumString);
                }
            }
        }
        public static void Grouping_linq_into(){
            IEnumerable<IGrouping<int,Obj>> GroupClients = //es un entero el parametro debido a que la key es entera
            //var GroupClients =
                from client in _Clients
                group client by client.Age into clientGroup
                where clientGroup.Count() >= 2
                select clientGroup
                ;
            foreach(IGrouping<int,Obj> groupClient in GroupClients){ //var = IGrouping<string,Obj>
                Console.WriteLine(groupClient.Key);
                foreach(Obj client in groupClient){
                    Console.WriteLine(client.Name+" "+client.Age+" "+client.Num+" "+client.NumString);
                }
            }
        }
        public static void Grouping_linq_into_withObject(){
            //IEnumerable<IGrouping<int,Obj>> GroupClients = //es un entero el parametro debido a que la key es entera
            var GroupClients =
                from client in _Clients
                group client by client.Age into clientGroup
                let sum = clientGroup.Sum(data => data.Num) //se suman los valores Num del objeto
                select new resultQuery{
                    Age = clientGroup.Key,
                    Count = clientGroup.Count(),
                    SumNum = sum
                }
                ;
            foreach(var Groupedclient in GroupClients){ //var = IGrouping<string,Obj>
                Console.WriteLine(Groupedclient.Age+" "+Groupedclient.SumNum+" "+Groupedclient.Count);
            }
        }
        public static void Grouping_linq_into_withArray(){
            int[] data = new int[]{2,5,8,9,4,6,5,6,3,4,4};
            var GroupedData = 
                from G_data in data
                group G_data by G_data into groupedData
                select new int[]{
                    groupedData.Key,
                    groupedData.Count()
                }
                ;  
        }
        public static void Grouping_linq_into_withObject_V2(){
            //IEnumerable<IGrouping<int,Obj>> GroupClients = //es un entero el parametro debido a que la key es entera
            var GroupClients =
                from client in _Clients
                group client by client.Age into clientGroup
                let sum = clientGroup.Sum(data => data.Num) //se suman los valores Num del objeto
                select new {
                    Age = clientGroup.Key,
                    Count_ = clientGroup.Count(),
                    SumNum_ = sum
                }
                ;
            foreach(var Groupedclient in GroupClients){ //var = IGrouping<string,Obj>
                Console.WriteLine(Groupedclient.Age+" "+Groupedclient.SumNum_+" "+Groupedclient.Count_);
            }
        }
        public static void Joining_Linq(){
            var JoinClient =
                from client in _Clients
                join destino in _Destino on client.Name equals destino.Name
                select new {
                    Name =client.Name,
                    Destino = destino.city
                };
                foreach(var joined in JoinClient){ //var = IGrouping<string,Obj>
                Console.WriteLine(joined.Name+" "+joined.Destino);
                }
        }
        
    
    }
    public class resultQuery {
        public int Age{get;set;}
        public int Count{get;set;}
        public int SumNum{get;set;}
    }
    public class Obj {
        public string Name {get;set;}
        public int Age {get;set;}
        public int Num {get;set;}
        public string NumString {get;set;}
    }
    public class Dest{
        public string Name {get;set;}
        public string city {get;set;}
    }
}
