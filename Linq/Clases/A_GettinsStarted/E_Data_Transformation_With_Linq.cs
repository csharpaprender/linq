using System;
using System.Collections.Generic;
using System.Linq;
namespace LinqBasics
{
    public class DataTransformationLinq{
        static List<Student> _Students;
        static List<Teacher> _Teachers;
        static DataTransformationLinq(){
            List<Student> students = new List<Student>{
                new Student { First="Svetlana",
                Last="Omelchenko",
                ID=111,
                Street="123 Main Street",
                City="Seattle",
                Scores= new List<int> { 97, 92, 81, 60 } },
            new Student { First="Claire",
                Last="O’Donnell",
                ID=112,
                Street="124 Main Street",
                City="Redmond",
                Scores= new List<int> { 75, 84, 91, 39 } },
            new Student { First="Sven",
                Last="Mortensen",
                ID=113,
                Street="125 Main Street",
                City="Lake City",
                Scores= new List<int> { 88, 94, 65, 91 } },
            };
            List<Teacher> teachers = new List<Teacher>()
            {
                new Teacher { First="Ann", Last="Beebe", ID=945, City="Seattle" },
                new Teacher { First="Alex", Last="Robinson", ID=956, City="Redmond" },
                new Teacher { First="Michiyo", Last="Sato", ID=972, City="Tacoma" }
            };
            _Students = students;
            _Teachers = teachers;
        }
        public static void UseConcat(){
            var EveryOne =
                (from student in _Students
                select new {
                    Last = student.Last,
                    ID = student.ID
                })
                .Concat( //concal es el equivalente a un union all de sql
                    from teacher in _Teachers
                    select new{
                        Last = teacher.Last,
                        ID = teacher.ID
                    }
                )
                ;
            foreach(var person in EveryOne){
                Console.WriteLine(person.Last+" "+person.ID);
            } 
        }
    
    
    }
    class Student
    {
        public string First { get; set; }
        public string Last {get; set;}
        public int ID { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public List<int> Scores;
    } 

    class Teacher
    {
        public string First { get; set; }
        public string Last { get; set; }
        public int ID { get; set; }
        public string City { get; set; }
    }
}