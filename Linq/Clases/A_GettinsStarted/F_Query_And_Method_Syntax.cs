using System;
using System.Collections.Generic;
using System.Linq;
namespace LinqBasics
{
    public class QueryAndMethod{
        public static void Query_VS_Method(){
            int[] numbers ={5,10,8,3,6,12};
            IEnumerable<int> numQuery =
                from number in numbers 
                where number%2==0
                orderby number
                select number;

            IEnumerable<int> numberMethod = 
                numbers.Where(n => n%2==0)
                .OrderBy(n => n);
            /*  Leer en la documentacion Methodos de Linq 
                para IEnumerable<>*/
            foreach(int n in numQuery){
                Console.WriteLine(n);
            }
            foreach(int n in numberMethod){
                Console.WriteLine(n);
            }
        }
    }

}