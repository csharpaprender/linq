﻿using System;
using System.Linq;
using LinqBasics;
using LinqBasics_W;
namespace Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            //BasicExpressions.BasicSelect();
            //IntroToLinq.ForceExecutionWitListArray();
            //LinqGeneric.useA_LinqGeneric();
            //BasicsLinq.Filtering_linq();
            //BasicsLinq.Ordering_linq();
            //BasicsLinq.Grouping_linq();
            //BasicsLinq.Grouping_linq_into();
            //BasicsLinq.Grouping_linq_into_withObject();
            //BasicsLinq.Grouping_linq_into_withArray();
            //BasicsLinq.Grouping_linq_into_withObject_V2();
            //BasicsLinq.Joining_Linq();
            //DataTransformationLinq.UseConcat();
            //QueryAndMethod.Query_VS_Method();
            Examples.GroupingQuery();



        }
    }
}
